import * as React from "react";

interface IMessageProps {
  text: string
}

interface IMessageState {
  isRead: boolean
}

export class Message extends React.Component<IMessageProps, IMessageState> {
  constructor(props) {
    super(props);

    this.state = {
      isRead: false
    };
  }

  onClickHandler() {
    this.setState(
      {
        isRead: true
      }
    );

    // console.log(`Clicks: ${this.state.clickCount}`);
  }

  render() {
    const isRead = this.state.isRead;
    const text = this.props.text;
    const className = "message " + (isRead ? "inactive" : "active");

    return (
      <div className={className} onClick={(e) => this.onClickHandler()}>
        <div>{text}</div>
        <div></div>
      </div>
    );
  }
}