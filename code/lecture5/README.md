# Task 4

Переписать код задачи 3, используя reactjs и redux

## Требования

1. Изменены настройки в `tsconfig.json` и `webpack.config.js`, необходимые для работы с react.
2. Задание 3 переписано на typescript/react/redux

Создать PR в `jstask1` ветку в своем `js` репозитории. (`jstask1` выбрана намеренно)
