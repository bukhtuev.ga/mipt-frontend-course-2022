function init() {
    function createList(count) 
    {
        let ul = document.createElement("ul");
        for (var i=1; i<=count; ++i)
        {
            let li = document.createElement("li");
            li.innerText = `Number ${i}`;
            li.onclick = (function(new_count){
                return () => alert(`You clicked ${new_count} element`);
            })(i);

            ul.append(li);
        }

        return ul;
    }

    document.getElementById("root").append(createList(5));


    // let request = new XMLHttpRequest();
    // request.onload = (e) => {
    //     let text = request.responseText;
    //     let data = JSON.parse(text);

    //     debugger;

    //     alert(data);
    // }

    // request.open("GET", "data.json");
    // request.send();

    const loadData = async () => {
        let response = await fetch("data.json");
        let data = await response.json();
        debugger;
        alert(data);
    }

    loadData();


}

window.onload = init;