(function(){
    let inp = document.createElement("input");
    inp.type = "text";
    inp.value = "Enter your name";

    let inpButton = document.createElement("input");
    inpButton.type = "button";
    inpButton.value = "Click Me!!"
    inpButton.addEventListener("click", (el, evnt) => {
        let request = new XMLHttpRequest();
        request.onload = (data) => {
            debugger;
            let el = JSON.parse(request.responseText);

            alert(el.array[0].name);
            
        }

        request.open("GET", "data.json");
        request.send();
    });

    // new button
    let buttunFetch = document.createElement("input");
    buttunFetch.type = "button";
    buttunFetch.value = "Click Me To Fetch!!"
    buttunFetch.addEventListener("click", async (el, evnt) => {
        let response = await fetch("data.json");
        let obj = await response.json();
        alert(JSON.stringify(obj));
    });


    let root = document.getElementById("content");
    root.append(inp, inpButton, buttunFetch);

    function createList() {
        let list = document.createElement("ul");
        list.append(...[1,2,3].map((num)=> { 
                let li = document.createElement("li"); 
                li.innerText = `Number ${num}`;
                li.id = `li_${num}`;

                return li;
            })
        );

        return list;
    }

    root.append(createList());

    var lis = document.getElementsByTagName("li");
    var num = 1;
    for (let el of lis) {
        let i = num;
        el.onclick = function() {alert(i);}
        num++;
    }


})()