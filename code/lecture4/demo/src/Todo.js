class Todo {
    constructor(action) {
        this.action = action;
    }
    do() {
        console.log(`I do: '${this.action}'`);
    }
}
export default Todo;