const Koa = require("Koa");
const app = new Koa();

// logger
app.use(async (ctx, next) => {
    await next();
    const rt = ctx.response.get('X-Response-Time');
    console.log(`${ctx.method} ${ctx.url} - ${rt}`);
});

// x-response-time
app.use(async (ctx, next) => {
    const start = Date.now();
    await next();
    const ms = Date.now() - start;
    ctx.set('X-Response-Time', `${ms}ms`);
});

// Auth
app.use(async (ctx, next) => {

    if (ctx.request.originalUrl == "/favicon.ico")
    {
        ctx.body = "iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAACJSURBVEhL7ZC9CYBADEbP2iEEp7JxAPdwBZex02EcQ/0iBCzC/ZlTkDx4RYrLC+cMwzB+QQ1bRRtYwSAbPJQdoRe6bIbS41x32MEg9NULlJakStEBRqMRT44yFF+htDRkdpTJiT+OMilxtSgTE1ePMr54sSgjxYtHmXv8tShD8Qn212QYxnc4dwKskJKEHrOFUQAAAABJRU5ErkJggg==";
        return;
    }

    
    if (ctx.query.apiCode == "secret")
    {
        await next();
    }
    else {
        ctx.body = '<html><head></head><body><h1>No Access</h1></body></html>';
        ctx.response.status = 401;
    }
});

// response
app.use(async ctx => {
    ctx.body = '<html><head></head><body><h1>Hello World</h1></body></html>';
 }
);


app.listen(3000);