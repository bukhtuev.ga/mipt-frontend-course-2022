# Task 3

Написать веб приложение для хостинка задачи 1. Вы можете использовать любой js webframework.

## Требования

1. Написан backend, который хостит html и scripts (css - пожеланию)
2. Задание 1 переписано на typescript
3. Используется webpack для транспиляции и добавления bundle в html.
4. Присутствует минимум 2 `npm` скрипта:
    1. `build` - создает bundle
    2. `start` - запускает web app


Создать PR в `jstask1` ветку в своем `js` репозитории. (`jstask1` выбрана намеренно)
