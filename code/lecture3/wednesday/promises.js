const print = (message) => console.log(message);

const todo = async () =>  {
    const p = Promise.resolve(123) /* .then(
        () => {
            ...
            () => {
                ...p.
            }
        }
    ); */
    const value = await p;
    print(value);
};

todo();