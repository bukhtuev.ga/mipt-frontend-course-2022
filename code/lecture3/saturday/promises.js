const http = () => Promise.resolve("{ \"data\": 123}");

const todo = async () => {
    const v = http("url1");
    const d = await http(v + "url2");

    console.log(d);
}

console.log(process.argv[2]);

todo();