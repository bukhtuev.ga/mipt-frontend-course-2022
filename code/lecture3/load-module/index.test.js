const FileStore  = require("./index");

jest.mock(FileStore);

test('FileStore save a file', () => {    
    const fileStore = new FileStore();
    const fileName = "fileName1";
    
    fileStore.save(fileName); 

    expect(fileStore.has(fileName)).toBe(true);
  });