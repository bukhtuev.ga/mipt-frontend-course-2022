# Syllabus

1. HTTP, HTML, CSS [14.09.2022, 17.09.2022]
2. JavaScript, JSON, DOM, AJAX [21.09.2022, 24.09.2022]
3. NodeJs & NPM [28.09.2022, 1.10.2022]
4. Best practicies, TypeScript, Webpack, Web Frameworks
5. ReactJs Essentials
6. ReduxJs Essentials. _Websocket_ [_optional_]

[https://jsfiddle.net/](https://jsfiddle.net/)
