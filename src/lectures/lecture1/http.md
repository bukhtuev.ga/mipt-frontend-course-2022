# HTTP

[https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol](https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol)

## Application Layer in OSI Model

![OSI MODEL](https://i.imgur.com/lqBDYqk.png)

## Client-Server Model

## URL Format

```
http[s]://host_name:[port]/resource/nested_resource/.../?p1=v1&p2=v2&...&pN=vN
```