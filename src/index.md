---
home: true
heroImage: https://v1.vuepress.vuejs.org/hero.png
tagline: Learning Web development with JS
actionText: Quick Start →
actionLink: /lectures/
features:
- title: JavaScript Basics
  details: JS Essentials 
- title: NodeJS & NPM
  details: Build any kind of applications 
- title: Web development with JS
  details: Approaches, Frameworks, Tools
footer: Made by Vladimir with ❤️
---
